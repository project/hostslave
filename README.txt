=== The Aegir hosting system === 
This install profile is part of the back end of a system for managing large amounts of hosted sites.

For more information and how to install this project, please read http://groups.drupal.org/aegir/overview

=== Hostmaster Layout ===
The installation profile should reside under the profiles directory under the drupal root and is named hostslave.

Modules Location:
Hostmaster requires modules to be installed in -
  profiles/hostslave/modules to avoid conflicts with different sites versions.

When installed your filesystem should look like this..

  profiles/hostslave/hostslave.profile
                     /modules/provision
                     /modules/cvs_deploy
                     /modules/drush


=== Pre Requirements ===
Required Modules:
  * provision
  * drush
  * cvs_deploy 
