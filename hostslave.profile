<?php

/**
 * Return an array of the modules to be enabled when this profile is installed.
 *
 * @return
 *  An array of modules to be enabled.
 */
function hostslave_profile_modules() {
  return array(
    /* core */ 'help', 'menu', 
    /* contrib */ 'drush', 'cvs_deploy',
    /* custom */ 'provision', 'provision_apache', 'provision_mysql', 'provision_drupal');
}

/**
 * Return a description of the profile for the initial installation screen.
 *
 * @return
 *   An array with keys 'name' and 'description' describing this profile.
 */
function hostslave_profile_details() {
  return array(
    'name' => 'HostSlave',
    'description' => 'Provides a headless installation of the provision module, that can be controlled using a Hostmaster frontend.'
  );
}


/**
 * Implementation of hook_form_alter().
 *
 * Allows the profile to alter the site-configuration form. This is
 * called through custom invocation, so $form_state is not populated.
function hostslave_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'install_configure') {
    drupal_rebuild_theme_registry();
    $form = array();
    $form['hello'] = array('#type' => 'markup', '#value' => provision_front());
    $form['submit'] = array('#type' => 'submit', '#value' => 'Continue'); 
  }
}
 */

/**
 * Perform any final installation tasks for this profile.
 *
 * @return
 *   An optional HTML string to display to the user on the final installation
 *   screen.
 */
function hostslave_profile_tasks(&$tasks, $url) {
  // @TODO : remove any and all functionality
  db_query("UPDATE {blocks} SET STATUS=0");
  variable_set("site_frontpage", "provision");
  variable_set("user_register", 0);
  $settings = variable_get("theme_settings", array());
  $settings['toggle_logo'] = 0;
  $settings['toggle_slogan'] = 0;
  $settings['toggle_name'] = 0;
  variable_set("theme_garland_settings", $settings);
  // This is saved because the config generation script is running via drush, and does not have access to this value
  variable_set('install_url' , $GLOBALS['base_url']);

  // Update the menu router information.
  menu_rebuild();
  
  drupal_goto('provision');
}

